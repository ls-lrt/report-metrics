TEX=metrics
BIB=bibmetrics

all:
	pdflatex $(TEX).tex
	bibtex $(TEX)
	pdflatex $(TEX).tex
	pdflatex $(TEX).tex

clean:
	rm bibmetrics.bib~ metrics.aux metrics.blg metrics.log metrics.tex~ bibmetrics.bib~ metrics.aux metrics.blg metrics.log metrics.tex~ metrics.bbl makefile~ metrics.toc metrics.out
